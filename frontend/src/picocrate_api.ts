import axios from "axios";

const BACKEND = process.env.NEXT_PUBLIC_PICOSTACK_BACKEND;

export type CatalogResource = {
    name: string;
}

export type CatalogSummaryResource = {
    id: string;
    name: string;
    stock: StockResource[];
}

export type StockResource = {
    location: string;
    amount: number;
}

export type AddStockResource = {
    location: string;
    amount: number;
}

export type RemoveStockResource = {
    location: string;
    amount: number;
}

export class PicoCrateApi {
    static async postCatalogItem(resource: CatalogResource) {
        await axios.post(BACKEND + "/catalog/items", resource);
    }

    static async putCatalogItemAddStock(id: string, resource: AddStockResource) {
        await axios.put(BACKEND + "/catalog/items/" + id + "/add-stock", resource);
    }

    static async putCatalogItemRemoveStock(id: string, resource: RemoveStockResource) {
        await axios
            .put(BACKEND + "/catalog/items/" + id + "/remove-stock", resource)
            .catch(error => { throw error.response.data.message });
    }

    static async getCatalogSummaries(): Promise<CatalogSummaryResource[]> {
        const response = await axios.get(BACKEND + "/catalog/summaries");
        return response.data;
    }

    static async getCatalogSummary(id: string): Promise<CatalogSummaryResource> {
        const response = await axios.get(BACKEND + "/catalog/summaries/" + id);
        return response.data;
    }
}