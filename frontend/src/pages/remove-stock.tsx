import { Alert, AlertIcon, Box, Heading } from "@chakra-ui/core";
import { Form, Formik } from "formik";
import { GetServerSideProps } from "next";
import { useRouter } from "next/router";
import React, { FC, useState } from "react";

import AppBar from "src/components/AppBar";
import CfNumberInput from "src/components/chakra-formik/number-input";
import CfSubmit from "src/components/chakra-formik/submit";
import CfTextInput from "src/components/chakra-formik/text-input";
import { CatalogSummaryResource, PicoCrateApi } from "src/picocrate_api";

type RemoveStockPageProps = {
    itemSummary: CatalogSummaryResource;
    location: string;
}

type RemoveStockValues = {
    location: string;
    amount: string;
}

const RetrieveStockPage: FC<RemoveStockPageProps> = props => {
    const [formError, setFormError] = useState<string>(null);
    const router = useRouter();

    const onValidate = (values: RemoveStockValues) => {
        const errors: any = {};

        if (values.amount == "") {
            errors.amount = "Amount is required.";
        } else if (!values.amount.match(/^\-?\d+$/)) {
            errors.amount = "Amount must be a number.";
        } else if (parseInt(values.amount) < 1) {
            errors.amount = "Amount must be at least 1.";
        }

        return errors;
    }

    const onSubmit = async (values: RemoveStockValues) => {
        try {
            await PicoCrateApi.putCatalogItemRemoveStock(
                props.itemSummary.id,
                {
                    location: values.location,
                    amount: parseInt(values.amount)
                }
            );

            router.push("/catalog/[id]", `/catalog/${props.itemSummary.id}`);
        } catch (error) {
            setFormError(error);
        }
    }

    return (
        <div>
            <AppBar />
            <Box p={4}>
                <Heading fontWeight="light">{props.itemSummary.name} - Remove Stock</Heading>
                {formError && <Alert status="error"><AlertIcon /> {formError}</Alert>}
                <Formik<RemoveStockValues>
                    initialValues={{ location: props.location, amount: "1" }}
                    onSubmit={onSubmit}
                    validate={onValidate}
                >
                    <Form>
                        <CfTextInput label="Location" name="location" disabled={true} />
                        <CfNumberInput label="Amount" name="amount" />
                        <CfSubmit>Remove</CfSubmit>
                    </Form>
                </Formik>
            </Box>
        </div>
    )
}

export default RetrieveStockPage;

export const getServerSideProps: GetServerSideProps<RemoveStockPageProps> = async context => {
    const itemSummary = await PicoCrateApi.getCatalogSummary(context.query.item as string);

    return {
        props: {
            itemSummary: itemSummary,
            location: context.query.location as string ?? "",
        }
    }
}
