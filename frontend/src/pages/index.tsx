import React, { FC, useState } from "react";
import { Box, Heading, Flex } from "@chakra-ui/core";
import Link from "next/link";
import { Formik, FormikHelpers, Form } from "formik";

import { CatalogSummaryResource, PicoCrateApi } from "src/picocrate_api";
import AppBar from "src/components/AppBar";
import CfTextInput from "src/components/chakra-formik/text-input";
import CfSubmit from "src/components/chakra-formik/submit";

type AddItemForm = {
    name: string;
}

const HomePage: FC = props => {
    const [catalogItems, setCatalogItems] = useState<CatalogSummaryResource[]>([]);

    const onAddSubmit = async (values: AddItemForm, helpers: FormikHelpers<AddItemForm>) => {
        await PicoCrateApi.postCatalogItem({ name: values.name });

        // Refresh the data on the page
        setCatalogItems(await PicoCrateApi.getCatalogSummaries());

        helpers.resetForm();
    }

    const catalogComps = catalogItems.map(v => (
        <Link href="/catalog/[id]" as={`/catalog/${v.id}`} key={v.id} passHref={true}>
            <Flex as="a" _odd={{ bg: "gray.100" }} _hover={{ bg: "gray.200" }}>
                <Box flexGrow={1} p={2}>{v.name}</Box>
                <Box width={100} p={2}>{v.stock.reduce((a, b) => a + b.amount, 0)}</Box>
                <Box width={100} p={2}>{v.stock.length}</Box>
            </Flex>
        </Link>
    ));

    return (
        <div>
            <AppBar />
            <Box p={4}>
                <Heading fontWeight="light">CATALOG</Heading>
                <Box bg="gray.50">
                    <Flex>
                        <Box flexGrow={1} p={2}>NAME</Box>
                        <Box width={100} p={2}>AMOUNT</Box>
                        <Box width={100} p={2}>LOCATIONS</Box>
                    </Flex>
                    {catalogComps}
                </Box>
                <Heading mt={4} size="lg" fontWeight="light">ADD CATALOG ITEM</Heading>
                <Formik<AddItemForm> initialValues={{ name: "" }} onSubmit={onAddSubmit}>
                    <Form>
                        <CfTextInput label="Name" name="name" />
                        <CfSubmit>Add</CfSubmit>
                    </Form>
                </Formik>
            </Box>
        </div>
    )
}

export default HomePage;
