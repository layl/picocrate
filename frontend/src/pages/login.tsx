import { Box, Button } from "@chakra-ui/core";
import Axios from "axios";
import { useRouter } from "next/router";
import React, { FC } from "react";
import AppBar from "src/components/AppBar";
import { useAuth } from "src/components/AuthProvider";

const login: FC = prop => {
    const router = useRouter();
    const auth = useAuth();

    const onLoginClicked = async () => {
        // Attempt the login (this will always succeed right now)
        const result = await Axios.post("http://localhost:8080/login", {});
        
        // Store the token
        auth.setToken(result.headers.authorization);

        // Go back to index
        router.push("/");
    }

    return (
        <div>
            <AppBar />
            <Box p={4}>
                <Button onClick={onLoginClicked}>Login</Button>
            </Box>
        </div>
    )
}

export default login;