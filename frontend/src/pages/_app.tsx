import Head from "next/head";
import { AppProps } from "next/app";
import { ChakraProvider } from "@chakra-ui/core";
import AuthProvider from "src/components/AuthProvider";

const MyApp = ({ Component, pageProps }: AppProps) => {
    return (
        <AuthProvider>
            <ChakraProvider resetCSS>
                <Head>
                    <link rel="icon" href="/favicon.ico" />
                    <title>PicoCrate</title>
                </Head>

                <Component {...pageProps} />
            </ChakraProvider>
        </AuthProvider>
    );
}

export default MyApp;
