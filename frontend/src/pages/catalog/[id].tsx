import { Box, Flex, Heading } from "@chakra-ui/core";
import { GetServerSideProps } from "next";
import React, { FC, useState } from "react";
import { Form, Formik, FormikHelpers } from "formik";
import Link from "next/link";

import { CatalogSummaryResource, PicoCrateApi } from "src/picocrate_api";
import AppBar from "src/components/AppBar";
import CfSubmit from "src/components/chakra-formik/submit";
import CfTextInput from "src/components/chakra-formik/text-input";
import CfNumberInput from "src/components/chakra-formik/number-input";

type CatalogItemPageProps = {
    catalogSummary: CatalogSummaryResource;
}

type AddStockValues = {
    location: string;
    amount: string;
}

const CatalogItemPage: FC<CatalogItemPageProps> = props => {
    const [catalogSummary, setCatalogSummary] = useState<CatalogSummaryResource>(
        props.catalogSummary
    );

    const onValidate = (values: AddStockValues) => {
        const errors: any = {};

        if (values.location == "") {
            errors.location = "Location is required.";
        }

        if (values.amount == "") {
            errors.amount = "Amount is required.";
        } else if (!values.amount.match(/^\-?\d+$/)) {
            errors.amount = "Amount must be a number.";
        } else if (parseInt(values.amount) < 1) {
            errors.amount = "Amount must be at least 1.";
        }

        return errors;
    }

    const onAddSubmit = async (values: AddStockValues, helpers: FormikHelpers<AddStockValues>) => {
        await PicoCrateApi.putCatalogItemAddStock(
            catalogSummary.id,
            {
                location: values.location,
                amount: parseInt(values.amount),
            }
        );

        // Refresh the data on the page
        setCatalogSummary(await PicoCrateApi.getCatalogSummary(catalogSummary.id));

        helpers.resetForm();
    }

    const stockComps = catalogSummary.stock.map(v => (
        <Link
            href={{
                pathname: "/remove-stock",
                query: {
                    item: catalogSummary.id,
                    location: v.location
                }
            }}
            passHref={true}
            key={v.location}
        >
            <Flex as="a" _odd={{ bg: "gray.100" }} _hover={{ bg: "gray.200" }}>
                <Box flexGrow={1} p={2}>{v.location}</Box>
                <Box width={100} p={2}>{v.amount}</Box>
            </Flex>
        </Link>
    ));

    return (
        <div>
            <AppBar />
            <Box p={4}>
                <Heading fontWeight="light">{catalogSummary.name}</Heading>
                <Heading mt={4} fontWeight="light" size="lg">Stock</Heading>
                <Box bg="gray.50">
                    <Flex>
                        <Box flexGrow={1} p={2}>LOCATION</Box>
                        <Box width={100} p={2}>AMOUNT</Box>
                    </Flex>
                    {stockComps}
                </Box>
                <Heading mt={4} fontWeight="light" size="lg">Add Stock</Heading>
                <Formik<AddStockValues>
                    initialValues={{ location: "", amount: "1" }}
                    onSubmit={onAddSubmit}
                    validate={onValidate}
                >
                    <Form>
                        <CfTextInput label="Location" name="location" />
                        <CfNumberInput label="Amount" name="amount" />
                        <CfSubmit>Add</CfSubmit>
                    </Form>
                </Formik>
            </Box>
        </div>
    )
}

export default CatalogItemPage;

export const getServerSideProps: GetServerSideProps<CatalogItemPageProps> = async context => {
    const catalogSummary = await PicoCrateApi.getCatalogSummary(context.query.id as string);

    return {
        props: { catalogSummary: catalogSummary }
    }
}
