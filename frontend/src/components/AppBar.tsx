import { Box } from "@chakra-ui/core";
import Link from "next/link";
import React, { FC } from "react";

const AppBar: FC = props => {
    return (
        <Box bg="blue.500" color="white">
            <Link href="/" passHref={true}>
                <Box p={4} as="a" display="inline-block" _hover={{bg: "blue.600"}}>
                    PICOCRATE
                </Box>
            </Link>
        </Box>
    );
}

export default AppBar;