import { NextRouter, useRouter } from "next/router";
import { createContext, FC, useContext, useEffect, useState } from "react";

type AuthContextType = {
    token: string | null;
    setToken: (token: string) => void;
};

export const AuthContext = createContext<AuthContextType>(null);

export const useAuth = () => useContext(AuthContext);

const validateRoute = (router: NextRouter, url: string, token: string | null) => {
    if (token == null && url !== "/login") {
        router.push("/login");
    }
}

const AuthProvider: FC = props => {
    const router = useRouter();
    const [token, setToken] = useState<string | null>(null);

    useEffect(() => {
        // Verify the current route
        validateRoute(router, router.pathname, token);

        // Handle changes in route
        const handleRouteChange = (url: string) => validateRoute(router, url, token);
        router.events.on("routeChangeStart", handleRouteChange);

        return () => {
            router.events.off("routeChangeStart", handleRouteChange);
        }
    }, [token]);

    const context = {
        token: token,
        setToken: (token: string) => {
            setToken(token);
        }
    };

    return <AuthContext.Provider value={context}>{props.children}</AuthContext.Provider>
}

export default AuthProvider;