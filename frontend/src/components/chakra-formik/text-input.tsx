import { FormControl, FormErrorMessage, FormLabel, Input } from "@chakra-ui/core";
import { FieldConfig, useField } from "formik";
import React, { FC } from "react";

type CfTextInputProps = FieldConfig & {
    label: string;
    disabled?: boolean;
}

const CfTextInput: FC<CfTextInputProps> = props => {
    const [field, meta] = useField(props);

    return (
        <FormControl isInvalid={meta.touched && !!meta.error}>
            <FormLabel>{props.label}</FormLabel>
            <Input type="text" disabled={props.disabled ?? false} {...field} />
            <FormErrorMessage>{meta.error}</FormErrorMessage>
        </FormControl>
    )
}

export default CfTextInput;