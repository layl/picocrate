import { FormControl, FormErrorMessage, FormLabel, NumberDecrementStepper, NumberIncrementStepper, NumberInput, NumberInputField, NumberInputStepper } from "@chakra-ui/core";
import { FieldConfig, useField } from "formik";
import React, { FC } from "react";

type CfNumberInputProps = FieldConfig & {
    label: string;
}

const CfNumberInput: FC<CfNumberInputProps> = props => {
    const [field, meta, helpers] = useField(props);

    return (
        <FormControl isInvalid={!!meta.error}>
            <FormLabel>{props.label}</FormLabel>
            <NumberInput {...field} onChange={value => helpers.setValue(value)}>
                <NumberInputField name={field.name} />
                <NumberInputStepper>
                    <NumberIncrementStepper />
                    <NumberDecrementStepper />
                </NumberInputStepper>
            </NumberInput>
            <FormErrorMessage>{meta.error}</FormErrorMessage>
        </FormControl>
    )
}

export default CfNumberInput;