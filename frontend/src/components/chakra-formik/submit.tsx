import { Button } from "@chakra-ui/core";
import { useFormikContext } from "formik";
import React, { FC } from "react";

const CfSubmit: FC = props => {
    const context = useFormikContext();

    return (
        <Button
            mt={2}
            type="submit"
            colorScheme="blue"
            disabled={context.isSubmitting}
            isLoading={context.isSubmitting}
            loadingText="Submitting..."
        >
            {props.children}
        </Button>
    )
}

export default CfSubmit;