package net.layl.picocrate

import org.springframework.security.core.userdetails.User
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.stereotype.Service

@Suppress("unused")
@Service
class UserDetailsServiceImpl(val bCryptPasswordEncoder: BCryptPasswordEncoder): UserDetailsService {
    override fun loadUserByUsername(username: String?): UserDetails {
        if (username != "username") {
            throw UsernameNotFoundException(username)
        }

        return User(username, bCryptPasswordEncoder.encode("password"), emptyList())
    }
}