package net.layl.picocrate.controllers

import net.layl.picocrate.*
import net.layl.picocrate.resources.AddStockResource
import net.layl.picocrate.resources.CatalogItemResource
import net.layl.picocrate.resources.CatalogSummaryResource
import net.layl.picocrate.resources.RemoveStockResource
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.*
import org.springframework.web.server.ResponseStatusException
import java.lang.IllegalArgumentException
import java.util.*

@Suppress("unused")
@RestController
@RequestMapping("/catalog")
class CatalogController(
        private val catalogService: CatalogService
) {
    @PostMapping("/items")
    fun postCatalogItem(@RequestBody resource: CatalogItemResource) {
        catalogService.addItem(resource.name)
    }

    @PutMapping("/items/{id}/add-stock")
    fun putCatalogItemAddStock(@PathVariable id: String, @RequestBody resource: AddStockResource) {
        val uuid = pathUuid(id)

        catalogService.addStock(uuid, resource.location, resource.amount)
    }

    @PutMapping("/items/{id}/remove-stock")
    fun putCatalogItemRemoveStock(@PathVariable id: String, @RequestBody resource: RemoveStockResource) {
        val uuid = pathUuid(id)

        catalogService.removeStock(uuid, resource.location, resource.amount)
    }

    @GetMapping("/summaries")
    fun getCatalogSummaries(): Collection<CatalogSummaryResource> {
        return catalogService.getSummaries()
    }

    @GetMapping("/summaries/{id}")
    fun getCatalogSummary(@PathVariable id: String): CatalogSummaryResource {
        val uuid = pathUuid(id)

        return catalogService.getSummary(uuid)
    }

    private fun pathUuid(id: String): UUID {
        try {
            return UUID.fromString(id)
        } catch (exception: IllegalArgumentException) {
            throw ResponseStatusException(HttpStatus.NOT_FOUND)
        }
    }
}