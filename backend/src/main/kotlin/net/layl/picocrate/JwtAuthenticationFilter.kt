package net.layl.picocrate

import com.auth0.jwt.JWT
import com.auth0.jwt.algorithms.Algorithm
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.core.Authentication
import org.springframework.security.core.userdetails.User
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter
import java.util.*
import javax.servlet.FilterChain
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

class JwtAuthenticationFilter(authenticationManager: AuthenticationManager) : UsernamePasswordAuthenticationFilter() {
    init {
        setAuthenticationManager(authenticationManager)
    }

    override fun obtainUsername(request: HttpServletRequest?): String? {
        return "username"
    }

    override fun obtainPassword(request: HttpServletRequest?): String? {
        return "password"
    }

    override fun successfulAuthentication(request: HttpServletRequest?, response: HttpServletResponse?, chain: FilterChain?, authResult: Authentication?) {
        val token = JWT.create()
                .withSubject((authResult?.principal as User).username)
                .withExpiresAt(Date(System.currentTimeMillis() + 864_000_000))
                // TODO: Configure in properties
                .sign(Algorithm.HMAC512("1245secret"))

        response?.addHeader("Authorization", "Bearer $token")
    }
}