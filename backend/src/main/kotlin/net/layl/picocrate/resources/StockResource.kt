package net.layl.picocrate.resources

data class StockResource(
        val location: String,
        val amount: Int
)