package net.layl.picocrate.resources

import java.util.*

data class CatalogSummaryResource(
        val id: UUID,
        val name: String,
        val stock: Collection<StockResource>
)