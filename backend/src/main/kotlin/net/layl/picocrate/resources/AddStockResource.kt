package net.layl.picocrate.resources

data class AddStockResource(
        val location: String,
        val amount: Int
)