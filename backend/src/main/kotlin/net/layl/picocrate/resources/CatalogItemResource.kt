package net.layl.picocrate.resources

data class CatalogItemResource(
        val name: String
)