package net.layl.picocrate.resources

data class RemoveStockResource(
        val location: String,
        val amount: Int
)