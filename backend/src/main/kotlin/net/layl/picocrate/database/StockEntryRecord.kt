package net.layl.picocrate.database

import java.util.*
import javax.persistence.*

@Entity
@Table(name = "stock_entries")
data class StockEntryRecord(
        @Column(name = "id")
        @Id
        var id: UUID,

        @ManyToOne
        @JoinColumn(name="item_id", nullable = false)
        var item: CatalogItemRecord,

        @Column(name = "location", nullable = false)
        var location: String,

        @Column(name = "amount", nullable = false)
        var amount: Int
)