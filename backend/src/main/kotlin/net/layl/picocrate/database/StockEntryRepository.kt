package net.layl.picocrate.database

import org.springframework.data.repository.CrudRepository
import java.util.*

interface StockEntryRepository: CrudRepository<StockEntryRecord, UUID>