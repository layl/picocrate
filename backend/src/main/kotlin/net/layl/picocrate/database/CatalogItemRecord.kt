package net.layl.picocrate.database

import java.util.*
import javax.persistence.*

@Entity
@Table(name = "catalog_items")
class CatalogItemRecord(
        @Column(name = "id")
        @Id
        var id: UUID,

        @Column(name = "name", nullable = false)
        var name: String
) {
        @OneToMany(mappedBy="item")
        var stockEntries: Set<StockEntryRecord> = emptySet()
}