package net.layl.picocrate

import com.auth0.jwt.JWT
import com.auth0.jwt.algorithms.Algorithm
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter
import javax.servlet.FilterChain
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

class JwtAuthorizationFilter(
        authenticationManager: AuthenticationManager
): BasicAuthenticationFilter(authenticationManager) {
    override fun doFilterInternal(request: HttpServletRequest, response: HttpServletResponse, chain: FilterChain) {
        val header = request.getHeader("Authorization")

        // TODO: Configure in properties
        val secret = "1245secret"

        // Make sure we've got an authorization token
        if (header == null || !header.startsWith("Bearer ")) {
            chain.doFilter(request, response)
            return
        }

        val rawToken = header.replace("Bearer ", "")

        // Parse the token
        val token = JWT.require(Algorithm.HMAC512(secret))
                .build()
                .verify(rawToken)

        // If we've got a user, add it to the context
        if (token.subject != null) {
            SecurityContextHolder.getContext().authentication = UsernamePasswordAuthenticationToken(token.subject, null)
        }

        chain.doFilter(request, response)
    }
}