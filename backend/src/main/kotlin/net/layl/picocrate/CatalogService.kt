package net.layl.picocrate

import net.layl.picocrate.database.CatalogItemRecord
import net.layl.picocrate.database.CatalogItemRepository
import net.layl.picocrate.database.StockEntryRecord
import net.layl.picocrate.database.StockEntryRepository
import net.layl.picocrate.resources.CatalogSummaryResource
import net.layl.picocrate.resources.StockResource
import org.springframework.data.repository.findByIdOrNull
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Isolation
import org.springframework.transaction.annotation.Transactional
import org.springframework.web.server.ResponseStatusException
import java.util.*

@Service
@Transactional
class CatalogService(
        private val catalogItemRepository: CatalogItemRepository,
        private val stockEntryRepository: StockEntryRepository
) {
    fun addItem(name: String): UUID {
        val item = CatalogItemRecord(UUID.randomUUID(), name)
        catalogItemRepository.save(item)
        return item.id
    }

    fun getSummaries(): Collection<CatalogSummaryResource> {
        return catalogItemRepository
                .findAll()
                .map { catalogItemRecordToSummary(it) }
    }

    fun getSummary(id: UUID): CatalogSummaryResource {
        val record = catalogItemRepository.findByIdOrNull(id) ?: throw ResponseStatusException(HttpStatus.NOT_FOUND)

        return catalogItemRecordToSummary(record)
    }

    @Transactional(isolation = Isolation.SERIALIZABLE)
    fun addStock(item: UUID, location: String, amount: Int) {
        if (amount < 1) {
            throw ResponseStatusException(HttpStatus.BAD_REQUEST, "Amount must be at least 1")
        }

        val itemRecord = catalogItemRepository.findByIdOrNull(item)
                ?: throw ResponseStatusException(HttpStatus.NOT_FOUND)

        // Search the existing stock entries in case there's already one for this location
        val entry = itemRecord.stockEntries.find { it.location == location }

        if (entry != null) {
            // Add to the existing location entry
            entry.amount += amount
        } else {
            // We did not find an existing location entry, so add a new one
            val record = StockEntryRecord(UUID.randomUUID(), itemRecord, location, amount)
            stockEntryRepository.save(record)
        }
    }

    @Transactional(isolation = Isolation.READ_COMMITTED)
    fun removeStock(item: UUID, location: String, amount: Int) {
        if (amount < 1) {
            throw ResponseStatusException(HttpStatus.BAD_REQUEST, "Amount must be at least 1")
        }

        val itemRecord = catalogItemRepository.findByIdOrNull(item)
                ?: throw ResponseStatusException(HttpStatus.NOT_FOUND)

        // Search for the stock entry
        val entry = itemRecord.stockEntries.find { it.location == location }
                ?: throw ResponseStatusException(HttpStatus.BAD_REQUEST, "Not enough in stock")

        // Make sure there's enough in stock of this item
        if (entry.amount < amount) {
            throw ResponseStatusException(HttpStatus.BAD_REQUEST, "Not enough in stock")
        }

        entry.amount -= amount
    }

    private fun catalogItemRecordToSummary(record: CatalogItemRecord): CatalogSummaryResource {
        val stock = record.stockEntries
                .map { StockResource(it.location, it.amount) }
                .sortedByDescending { it.amount }

        return CatalogSummaryResource(record.id, record.name, stock)
    }
}