package net.layl.picocrate

import org.assertj.core.api.Assertions.*
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.HttpStatus
import org.springframework.web.server.ResponseStatusException

@SpringBootTest
class CatalogServiceTests {
    @Autowired
    private lateinit var catalogService: CatalogService

    @Test
    fun contextLoads() {
        assertThat(catalogService).isNotNull
    }

    @Test
    fun addStock() {
        // Arrange
        val id = catalogService.addItem("Item")

        // Act
        catalogService.addStock(id, "Location", 5)

        // Assert
        val summary = catalogService.getSummary(id)
        assertThat(summary.stock.size).isEqualTo(1)
    }

    @Test
    fun addStockWithExisting() {
        // Arrange
        val id = catalogService.addItem("Item")
        catalogService.addStock(id, "Location", 2)

        // Act
        catalogService.addStock(id, "Location", 5)

        // Assert
        val summary = catalogService.getSummary(id)
        assertThat(summary.stock.size).isEqualTo(1)
        assertThat(summary.stock.first().amount).isEqualTo(7)
    }

    @Test
    fun removeStock() {
        // Arrange
        val id = catalogService.addItem("Test")
        catalogService.addStock(id, "Location", 6)

        // Act
        catalogService.removeStock(id, "Location", 4)

        // Assert
        val summary = catalogService.getSummary(id)
        val location = summary.stock.find { it.location == "Location" }
        assertThat(location).isNotNull
        assertThat(location?.amount).isEqualTo(2)
    }

    @Test
    fun removeStockWithInsufficient() {
        // Arrange
        val id = catalogService.addItem("Test")

        // Act
        val thrown = catchThrowable {
            catalogService.removeStock(id, "Location", 4)
        }

        // Assert
        assertThat(thrown).isInstanceOf(ResponseStatusException::class.java)
        val exception = thrown as ResponseStatusException
        assertThat(exception.status).isEqualTo(HttpStatus.BAD_REQUEST)
    }
}