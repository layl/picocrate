# PicoCrate

A small inventory tracker, written as a portfolio example.


## Development Setup

### Frontend

```
cd frontend
yarn install
yarn run dev
```


### Backend

Set the the spring profile to "dev" using an environment variable.
You can do this in your IDE of choice.

```
SPRING_PROFILES_ACTIVE=dev
```

Copy "application-example.properties" to "application-dev.properties" and adjust the values as
necessary for your local setup.
